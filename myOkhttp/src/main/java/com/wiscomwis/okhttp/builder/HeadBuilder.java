package com.wiscomwis.okhttp.builder;


import com.wiscomwis.okhttp.OkHttpHelper;
import com.wiscomwis.okhttp.request.CommonRequest;
import com.wiscomwis.okhttp.request.OtherRequest;

/**
 * Created by zhangdroid on 2017/5/20.
 */
public class HeadBuilder extends GetBuilder {

    @Override
    public CommonRequest build() {
        return new OtherRequest(null, null, OkHttpHelper.METHOD.HEAD, url, tag, params, headers, id).build();
    }

}
