package com.wiscomwis.jimo.wxapi;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.common.HyphenateHelper;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.HostInfo;
import com.wiscomwis.jimo.data.model.Login;
import com.wiscomwis.jimo.data.model.Oauth;
import com.wiscomwis.jimo.data.model.Register;
import com.wiscomwis.jimo.data.model.UserBase;
import com.wiscomwis.jimo.data.model.UserBean;
import com.wiscomwis.jimo.data.model.UserDetail;
import com.wiscomwis.jimo.data.model.UserInfo;
import com.wiscomwis.jimo.data.preference.AnchorPreference;
import com.wiscomwis.jimo.data.preference.BeanPreference;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.jimo.db.DbModle;
import com.wiscomwis.jimo.event.FinishEvent;
import com.wiscomwis.jimo.ui.main.MainActivity;
import com.wiscomwis.library.dialog.LoadingDialog;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.ToastUtil;
import com.wiscomwis.okhttp.OkHttpHelper;
import com.wiscomwis.okhttp.callback.JsonCallback;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import okhttp3.Call;

public class WXEntryActivity extends AppCompatActivity implements IWXAPIEventHandler {
    // IWXAPI 是第三方app和微信通信的openapi接口
    private IWXAPI api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wx_entry);
        initViews();
    }

    protected void initViews() {
        // 通过WXAPIFactory工厂，获取IWXAPI的实例
        api = WXAPIFactory.createWXAPI(this, "wx557a81556f6683a0", false);
        // 将该app注册到微信
        api.registerApp("wx557a81556f6683a0");
        api.handleIntent(getIntent(), this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }

    // 微信发送请求到第三方应用时，会回调到该方法
    @Override
    public void onReq(BaseReq baseReq) {

    }


    // 第三方应用发送到微信的请求处理后的响应结果，会回调到该方法
    @Override
    public void onResp(BaseResp resp) {
        switch (resp.errCode) {
            case BaseResp.ErrCode.ERR_OK:
                String code = ((SendAuth.Resp) resp).code;
                getAccessToken(code);
                break;
            case BaseResp.ErrCode.ERR_AUTH_DENIED://用户拒绝授权
                finish();
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL://用户取消授权
                finish();
                break;
            default:
                finish();
                break;
        }
    }

    private void getAccessToken(String code) {
        LoadingDialog.show(getSupportFragmentManager());
        String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx557a81556f6683a0" +
                "&secret=" + "d90c6d892059bc34589feef1c7209893" +
                "&code=" + code + "&grant_type=authorization_code";
        OkHttpHelper.post()
                .url(url)
                .build()
                .execute(new JsonCallback<Oauth>() {
                             @Override
                             public void onError(Call call, Exception e, int id) {
                                 finish();
                             }

                             public void onSuccess(Oauth response, int id) {
                                 Log.e("getAccessToken", response.toString());
                                 if (response != null && !TextUtils.isEmpty(response.getAccess_token())) {
                                     String access_token = response.getAccess_token();
                                     String openid = response.getOpenid();
                                     getUserInfo(access_token, openid);
                                 } else {
                                     finish();
                                 }
                             }
                         }
                );
    }

    private void getUserInfo(String access, String openId) {
        String url = "https://api.weixin.qq.com/sns/userinfo?access_token=" + access + "&openid=" + openId;
        OkHttpHelper.post()
                .url(url)
                .build()
                .execute(new JsonCallback<UserInfo>() {
                             @Override
                             public void onError(Call call, Exception e, int id) {
                                 Log.e("getUserInfo", "onError" + e.getMessage());
                                 finish();
                             }

                             public void onSuccess(UserInfo response, int id) {
                                 Log.e("getUserInfo", response.toString());
                                 if (response != null && !TextUtils.isEmpty(response.getNickname())) {
                                     login(response.getOpenid(), response.getNickname(), response.getSex(), response.getHeadimgurl());
                                 } else {
                                     finish();
                                 }
                             }
                         }
                );
    }

    private void login(final String openId, final String nickName, final String sex, final String headImageUrl) {
        ApiManager.wxLogin("", "", openId, new IGetDataListener<Login>() {
            @Override
            public void onResult(Login login, boolean isEmpty) {
                Log.e("wxLogin", login.toString());
                if (login != null) {
                    if (login.getIsSucceed().equals("1")) {//登录成功
                        UserDetail userDetail = login.getUserDetail();
                        if (null != userDetail) {
                            HyphenateHelper.getInstance().clearAllUnReadMsg();//清空所有未读消息
                            DbModle.getInstance().getUserAccountDao().deleteAllData();//删除数据库中的所有数据
                            DbModle.getInstance().getUserAccountDao().deleteQaAllData();//删除QA
                            // 保存用户相关信息
                            UserBean userBean = userDetail.getUserBean();
                            if (null != userBean) {
                                BeanPreference.saveUserBean(userBean);
                            }
                            HostInfo hostInfo = userDetail.getHostInfo();
                            if (null != hostInfo) {
                                AnchorPreference.saveHostInfo(hostInfo);
                            }
                            UserBase userBase = userDetail.getUserBase();
                            if (null != userBase) {
                                // 保存用户信息到本地
                                UserPreference.saveUserInfo(userBase);
                                // 登录环信
                                ToastUtil.showShortToast(WXEntryActivity.this, login.getMsg());
                                handleHyphenateLoginResult();
                            } else {
                                ToastUtil.showShortToast(WXEntryActivity.this, login.getMsg());
                                handleHyphenateLoginResult();
                            }
                        } else {
                            finish();
                        }
                    } else if (login.getIsSucceed().equals("-3")) {//用户不存在
                        download(headImageUrl, nickName, sex, openId);
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                Log.e("wxLogin", "onError" + msg);
                finish();
            }
        });
    }

    private void handleHyphenateLoginResult() {
        LoadingDialog.hide();
        // 关闭之前打开的页面
        EventBus.getDefault().post(new FinishEvent());
        // 跳转主页面
        LaunchHelper.getInstance().launchFinish(WXEntryActivity.this, MainActivity.class);
    }

    private void download(final String headimgurl, final String nickName, final String sex, final String openId) {
        new AsyncTask<Void, Void, File>() {
            @Override
            protected File doInBackground(Void... params) {
                try {
                    return Glide.with(WXEntryActivity.this)
                            .load(headimgurl)
                            .downloadOnly(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                            .get();
                } catch (Exception ex) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(File file) {
                if (file == null || !file.exists()) {
                    finish();
                    return;
                }
                renameFile(nickName, sex, file, openId);
            }
        }.execute();
    }

    private void renameFile(String nickName, String sex, File file, String openId) {
        if (!file.exists() || file.isDirectory()) {
            finish();
            return;
        }
        String toFile = file.getPath() + "photo.jpg";
        File newFile = new File(toFile);

        //修改文件名
        if (file.renameTo(newFile)) {
            register(nickName, sex, newFile, openId);
        } else {
            finish();
        }


    }

    private void register(String nickName, String sex, File file, String openId) {
        boolean isMale = !sex.equals("2");
        String content = Util.encodeHeadInfo(nickName.toString().trim());
        ApiManager.photoRegister(content, isMale, file, openId,
                new IGetDataListener<Register>() {
                    @Override
                    public void onResult(Register register, boolean isEmpty) {
                        LoadingDialog.hide();
                        // 保存用户信息到本地
                        UserBase userBase = register.getUserBase();
                        if (null != userBase) {
                            UserPreference.saveUserInfo(userBase);
                        }
                        DbModle.getInstance().getUserAccountDao().deleteAllData();//删除数据库中的所有数据
                        // 关闭之前打开的页面
                        EventBus.getDefault().post(new FinishEvent());
                        // 跳转主页面
                        LaunchHelper.getInstance().launchFinish(WXEntryActivity.this, MainActivity.class);
                        UserPreference.registered();//注册成功的标记
                    }

                    @Override
                    public void onError(String msg, boolean isNetworkError) {
                        LoadingDialog.hide();
                        if (isNetworkError) {
                            ToastUtil.showShortToast(WXEntryActivity.this, getString(R.string.tip_network_error));
                        } else {
                            ToastUtil.showShortToast(WXEntryActivity.this, TextUtils.isEmpty(msg) ? getString(R.string.tip_empty) : msg);
                        }
                        finish();
                    }
                });
    }
}