package com.wiscomwis.jimo.ui.charmandrankinglist;

import android.content.Context;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseFragment;
import com.wiscomwis.jimo.ui.charmandrankinglist.contract.VideoListContract;
import com.wiscomwis.jimo.ui.charmandrankinglist.presenter.VideoListPresenter;
import com.wiscomwis.library.widget.VerticalViewPager;

import butterknife.BindView;

public class VideoListFragment extends BaseFragment implements VideoListContract.IView {
    @BindView(R.id.vvp_video_list)
    VerticalViewPager viewpager;
    @BindView(R.id.rl_more)
    RelativeLayout rl_more;
    VideoListPresenter videoListPresenter;
    private int mPostion = 1;
    private int i=0;
    private boolean isposition=true;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_video_list_layout;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected View getNoticeView() {
        viewpager.setOffscreenPageLimit(2);
        return viewpager;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    @Override
    public void hideRefresh(int delaySeconds) {

    }

    @Override
    protected void initViews() {
        videoListPresenter = new VideoListPresenter(this);
        videoListPresenter.start();
    }

    @Override
    protected void setListeners() {
        rl_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoListPresenter.reportShow(viewpager.getCurrentItem());
            }
        });

        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int positionOffsetPixels1 = positionOffsetPixels;
                if(position==0&&positionOffsetPixels1==0){
                    i++;
                }
                if(i>10&&isposition){
                    isposition=false;
                    i=0;
                    videoListPresenter.loadVideoShowList(100);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isposition=true;
                        }
                    },5000);
                }
            }

            @Override
            public void onPageSelected(int position) {

                if (viewpager.getAdapter().getCount() - 1 == position) {
                    videoListPresenter.loadVideoShowList(mPostion);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void loadData() {
        videoListPresenter.loadVideoShowList(1);
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            if (viewpager.getAdapter().getCount() == 0) {
                videoListPresenter.loadVideoShowList(1);
            }
        }
    }

    @Override
    public void showTip(String msg) {

    }

    @Override
    public void setAdapter(FragmentPagerAdapter adapter) {
        if(viewpager!=null){
            viewpager.setAdapter(adapter);
        }

    }

    @Override
    public void setEmptyView(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowEmpty(false, null, null);
                videoListPresenter.loadVideoShowList(1);
            }
        });
    }

    @Override
    public void setErrorView(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                videoListPresenter.loadVideoShowList(1);
            }
        });
    }

    @Override
    public Fragment getFragment() {
        return VideoListFragment.this;
    }

    @Override
    public void setPostion() {
        mPostion++;
    }

    @Override
    public void clearPosition() {
        mPostion=1;
    }
}
