package com.wiscomwis.jimo.ui.pay.activity;

import android.content.Context;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseFragmentActivity;
import com.wiscomwis.jimo.data.preference.DataPreference;
import com.wiscomwis.jimo.event.SingleLoginFinishEvent;
import com.wiscomwis.jimo.parcelable.ChatParcelable;
import com.wiscomwis.jimo.ui.chat.ChatActivity;
import com.wiscomwis.jimo.ui.homepage.HomepageAdapter;
import com.wiscomwis.jimo.ui.pay.adapter.PayAdapter;
import com.wiscomwis.jimo.ui.pay.contract.RechargeContract;
import com.wiscomwis.jimo.ui.pay.fragment.IncomeDetailFragment;
import com.wiscomwis.jimo.ui.pay.fragment.WithdrawalRecordFragment;
import com.wiscomwis.jimo.ui.pay.presenter.RechargePresenter;
import com.wiscomwis.jimo.ui.personalcenter.TvBalanceParcelable;
import com.wiscomwis.jimo.ui.personalcenter.WithdrawActivity;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.SnackBarUtil;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;

/**
 * Created by WangYong on 2017/9/2.
 */

public class MyWalletActivity extends BaseFragmentActivity implements View.OnClickListener, RechargeContract.IView {
    @BindView(R.id.mywallet_activity_vp)
    ViewPager mViewPager;
    @BindView(R.id.mywallet_activity_ll_root)
    LinearLayout mRoot;
    @BindView(R.id.mywallet_activity_tab)
    TabLayout mTab;
    @BindView(R.id.mywallet_activity_tv_explain)
    TextView tv_explain;
    @BindView(R.id.mywallet_activity_rl_back)
    RelativeLayout rl_back;
    @BindView(R.id.mywallet_activity_btn_tixian)
    Button btn_tixian;
    @BindView(R.id.mywallet_activity_tv_money)
    TextView tv_money;
    RechargePresenter mPresenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_mywallet;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        HomepageAdapter homepageAdapter = new HomepageAdapter(getSupportFragmentManager());
        List<Fragment> fragmentList = new ArrayList<>();
        List<String> tabList = Arrays.asList(getResources().getString(R.string.income), getResources().getString(R.string.person_withdraw_record));
        fragmentList.add(new IncomeDetailFragment());
        fragmentList.add(new WithdrawalRecordFragment());
        homepageAdapter.setData(fragmentList, tabList);
        mViewPager.setAdapter(homepageAdapter);
        mTab.setupWithViewPager(mViewPager);
        mPresenter = new RechargePresenter(this);
    }

    @Override
    protected void setListeners() {
        tv_explain.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        btn_tixian.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mywallet_activity_tv_explain://客服
//                LaunchHelper.getInstance().launch(mContext, WithdrawExplainActivity.class);
                LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(10000,
                        "100010000", mContext.getString(R.string.kefu_desc), DataPreference.getSystemIcon(), 0));
                break;
            case R.id.mywallet_activity_rl_back:
                finish();
                break;
            case R.id.mywallet_activity_btn_tixian:
                String s = tv_money.getText().toString();
                if (s != null && s.length() > 0) {
                    int v1 = Integer.parseInt(tv_money.getText().toString());
                    if (v1 < 2000) {
                        Toast.makeText(MyWalletActivity.this, getString(R.string.minimum_withdraw), Toast.LENGTH_SHORT).show();
                    } else {
                        LaunchHelper.getInstance().launch(mContext, WithdrawActivity.class,
                                new TvBalanceParcelable(v1));
                    }
                }
                break;
        }
    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {
        SnackBarUtil.showShort(mRoot, msg);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }

    @Override
    public void showNetworkError() {

    }

    @Override
    public void setAdapter(PagerAdapter pagerAdapter) {

    }

    @Override
    public FragmentManager getManager() {
        return null;
    }

    @Override
    public void setAdapter(PayAdapter payAdapter) {

    }

    @Override
    public void setTextDetail(String detial) {

    }

    @Override
    public void getKeyNum(String num) {

    }

    @Override
    public void getNowMoney(String money) {
        tv_money.setText(money);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getMyInfo();
    }

    @Subscribe
    public void onEvent(SingleLoginFinishEvent event) {
        finish();//单点登录销毁的activity
    }

    @Override
    public void isVipUser() {

    }
}
