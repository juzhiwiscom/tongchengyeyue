package com.wiscomwis.jimo.ui.charmandrankinglist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseFragment;
import com.wiscomwis.jimo.common.CustomDialogAboutOther;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.common.VideoHelper;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.BaseModel;
import com.wiscomwis.jimo.data.model.CheckStatus;
import com.wiscomwis.jimo.data.model.VideoSquare;
import com.wiscomwis.jimo.data.preference.DataPreference;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.jimo.event.GiftSendEvent;
import com.wiscomwis.jimo.event.IsFollow;
import com.wiscomwis.jimo.event.PauseEvent;
import com.wiscomwis.jimo.event.StartEvent;
import com.wiscomwis.jimo.event.StartVideoEvent;
import com.wiscomwis.jimo.event.UpdataFollowUser;
import com.wiscomwis.jimo.parcelable.ChatParcelable;
import com.wiscomwis.jimo.parcelable.UserDetailParcelable;
import com.wiscomwis.jimo.parcelable.VideoInviteParcelable;
import com.wiscomwis.jimo.parcelable.VideoShowParcelable;
import com.wiscomwis.jimo.ui.chat.ChatActivity;
import com.wiscomwis.jimo.ui.detail.UserDetailActivity;
import com.wiscomwis.jimo.ui.main.MainActivity;
import com.wiscomwis.jimo.ui.personalcenter.AuthenticationActivity;
import com.wiscomwis.jimo.ui.personalcenter.VideoShowActivity;
import com.wiscomwis.library.dialog.AlertDialog;
import com.wiscomwis.library.dialog.OnDialogClickListener;
import com.wiscomwis.library.image.CropCircleTransformation;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.LaunchHelper;
import com.wiscomwis.library.util.LogUtil;
import com.wiscomwis.library.util.SharedPreferenceUtil;
import com.wiscomwis.library.util.ToastUtil;
import com.wiscomwis.playerlibrary.EmptyControlVideo;
import com.wiscomwis.playerlibrary.gsyvideoplayer.GSYVideoManager;
import com.wiscomwis.playerlibrary.gsyvideoplayer.listener.GSYSampleCallBack;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import butterknife.BindView;

/**
 * 待整理，优化
 * Created by xuzhaole on 2018/3/20.
 */

public class VideoRoomFragment extends BaseFragment implements View.OnClickListener {
    @BindView(R.id.player_empty_room)
    EmptyControlVideo player_empty_room;
    @BindView(R.id.iv_avatar)
    ImageView ivAvatar;
    @BindView(R.id.rl_avatar)
    RelativeLayout rlAvatar;
    @BindView(R.id.ll_chat)
    LinearLayout llChat;
    @BindView(R.id.iv_video)
    ImageView ivVideo;
    @BindView(R.id.rl_video)
    RelativeLayout rlVideo;
    @BindView(R.id.iv_send_gift)
    ImageView ivSendGift;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_account)
    TextView tvAccount;
    @BindView(R.id.iv_cover_photo)
    ImageView iv_cover_photo;
    @BindView(R.id.iv_follow)
    ImageView iv_follow;
    @BindView(R.id.iv_bg_show)
    ImageView ivBgShow;
    @BindView(R.id.progress_empty)
    ProgressBar progressEmpty;
    @BindView(R.id.fl_empty)
    FrameLayout flEmpty;


    private VideoSquare mData;
    protected boolean isInit = false;
    protected boolean isLoad = false;
    private long lastClickTime = 0;
    private String cacheImg;
    private Bitmap bmp;
    private boolean isRelease=false;


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Log.v("播放视频==", "setUserVisibleHint-1=" + isVisibleToUser);
            initView();
        } else {
            if (iv_cover_photo != null && player_empty_room != null && !TextUtils.isEmpty(cacheImg)) {
                Log.v("播放视频==", "setUserVisibleHint-1=执行隐藏功能");
                flEmpty.setVisibility(View.VISIBLE);
            }

        }
    }

    private void playVideoShow() {
        player_empty_room.setLooping(true);
        player_empty_room.setUp(mData.gettUserVideoShow().getVideoUrl(), true, "123");
        player_empty_room.startPlayLogic();
        player_empty_room.setVideoAllCallBack(new GSYSampleCallBack() {
            //加载成功，
            @Override
            public void onPrepared(String url, Object... objects) {
                super.onPrepared(url, objects);
                if((!getUserVisibleHint()||getActivityPosition()!=1)&&player_empty_room != null)
                    GSYVideoManager.instance().onPause();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(flEmpty!=null)
                            flEmpty.setVisibility(View.GONE);
                        LogUtil.v("cacheImg","开始播放视频=="+cacheImg);
                        Log.v("播放视频==", "加载成功progressEmpty=");
                    }
                },200);
            }
        });

    }

    private int getActivityPosition() {
        MainActivity mainActivity = (MainActivity) getActivity();
        return mainActivity.getPosition();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() && getActivityPosition() == 1) {
            Log.v("播放视频==", "onResume-=");
            if(isRelease){
                isRelease=false;
                initViews();
            }else {
                playVideoShow();
            }

        }
    }

    @Override
    public void onPause() {
        if (getUserVisibleHint() && getActivityPosition() == 1){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.v("播放视频==", "onPause-=");
                    GSYVideoManager.instance().onPause();
                    GSYVideoManager.instance().setNeedMute(true);
                    if(player_empty_room!=null)
                        player_empty_room.onVideoPause();
                    isRelease=true;
                }
            },200);
        }
        super.onPause();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isLoad = false;
        isInit = false;
    }

    @Override
    protected void initViews() {
        isInit = true;
        if (mData != null) {
            String iconUrl = mData.gettUserVideoShow().getVideoUrl();
            if (!TextUtils.isEmpty(iconUrl)) {
                cacheImg = SharedPreferenceUtil.getStringValue(mContext, "cacheImg", iconUrl, "url");
                LogUtil.v("cacheImg","获取视频第一帧=="+cacheImg);

                if (!cacheImg.equals("url")) {
                    bmp = BitmapFactory.decodeFile(cacheImg);
//                    player_empty_room.setBg(bmp);

                    iv_cover_photo.setImageBitmap(bmp);
                    progressEmpty.setVisibility(View.VISIBLE);
                    iv_cover_photo.setVisibility(View.VISIBLE);
//                    player_empty_room.setVisibility(View.GONE);
                }else {
                    new getVideoCacheAsyncTask(mContext).execute(iconUrl);
                }
            }
        }
        initView();
    }

    protected void initView() {
        if (!isInit) {
            return;
        }
        if (getUserVisibleHint()) {
            if (mData != null && mData.getUserBase() != null) {
                tvName.setText(mData.getUserBase().getNickName());
                tvAccount.setText(mData.getUserBase().getOwnWords());
            }
            if (mData != null && mData.gettUserVideoShow() != null) {
                if (mData.getIsFollow() == 1) {
                    iv_follow.setVisibility(View.GONE);
                } else {
                    iv_follow.setVisibility(View.VISIBLE);
                }
                ImageLoaderUtil.getInstance().loadImage(this, new ImageLoader.Builder()
                        .url(mData.getUserBase().getIconUrlMininum())
                        .transform(new CropCircleTransformation(mContext))
                        .placeHolder(Util.getDefaultImageCircle()).error(Util.getDefaultImageCircle()).imageView(ivAvatar).build());

                playVideoShow();
                isLoad = true;
            }
        }
    }


    @Override
    protected void setListeners() {
        rlAvatar.setOnClickListener(this);
        rlVideo.setOnClickListener(this);
        llChat.setOnClickListener(this);
        ivSendGift.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }


    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_video_show;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void getArgumentParcelable(Parcelable parcelable) {

    }

    public void setData(VideoSquare t) {
        this.mData = t;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_avatar://头像
                if (mData == null) {
                    return;
                }
                if (UserPreference.isAnchor()) {
                    ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                    return;
                }
                if (mData.getIsFollow() == 1) {//已关注，进入到详情
                    LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                            new UserDetailParcelable(String.valueOf(mData.getUserBase().getGuid())));
                } else {//未关注，直接关注
                    follow();
                }
                break;
            case R.id.rl_video://视频
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    if (UserPreference.isAnchor()) {
                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                        return;
                    }
                    goToVideoInvite();
                    if(UserPreference.isVip()){//付费
                        getClickNum(String.valueOf(mData.getUserBase().getGuid()),"1","12");
                    }else{//未付费
                        getClickNum(String.valueOf(mData.getUserBase().getGuid()),"2","12");
                    }
                }

                break;
            case R.id.ll_chat://聊天
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    if (UserPreference.isAnchor()) {
                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                        return;
                    }

                    //跳转写信的界面
                    goToWriteMessage();
                    if(UserPreference.isVip()){//付费
                        getClickNum(String.valueOf(mData.getUserBase().getGuid()),"1","12");
                    }else{//未付费
                        getClickNum(String.valueOf(mData.getUserBase().getGuid()),"2","14");
                    }
                }
                break;
            case R.id.iv_send_gift://送礼物
                if (System.currentTimeMillis() - lastClickTime > 1000) {
                    lastClickTime = System.currentTimeMillis();
                    if (UserPreference.isAnchor()) {
                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.raising_profile));
                        return;
                    }
                    CustomDialogAboutOther.giveGiftShow(mContext, mData.getUserBase().getGuid() + "", mData.getUserBase().getAccount(), mData.getUserBase().getIconUrlMininum(), mData.getUserBase().getNickName(), 1, false);
              if(UserPreference.isVip()){//付费
                  getClickNum(String.valueOf(mData.getUserBase().getGuid()),"1","14");
              }else{//未付费
                  getClickNum(String.valueOf(mData.getUserBase().getGuid()),"2","14");
              }

                }
                break;
        }
    }

    private void follow() {
        ApiManager.follow(mData.getUserBase().getGuid() + "", new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                EventBus.getDefault().post(new IsFollow(mData.getUserBase().getAccount()));
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                ToastUtil.showShortToast(mContext, mContext.getString(R.string.follow_fail));
            }
        });
    }

    private void goToVideoInvite() {
        if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
            getStatus();
        } else {
            if (UserPreference.getStatus().equals("3")) {
                Toast.makeText(mContext, mContext.getString(R.string.quiet_hours_me), Toast.LENGTH_SHORT).show();
            } else {
                if (mData.getUserBase() != null) {
                    VideoHelper.startVideoInvite(new VideoInviteParcelable(false, mData.getUserBase().getGuid(), mData.getUserBase().getAccount()
                            , mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0, 0), mContext, "1");
                }
            }
        }
    }

    private void getStatus() {
        ApiManager.getAuthorCheckStatus(new IGetDataListener<CheckStatus>() {
            @Override
            public void onResult(CheckStatus checkStatus, boolean isEmpty) {
                if (checkStatus != null) {
                    if (checkStatus.getAuditStatus() == 0) {//审核中
                        if (checkStatus.getShowStatus() == 1) {//完整
                            Toast.makeText(mContext, "认证审核中.....", Toast.LENGTH_SHORT).show();
                        } else if (checkStatus.getShowStatus() == -1) {//不完整
                            AlertDialog.showNoCanceled(getFragmentManager(), "", "视频聊天需要通过真人认证哦",
                                    "去认证", "取消", new OnDialogClickListener() {
                                        @Override
                                        public void onNegativeClick(View view) {
                                        }

                                        @Override
                                        public void onPositiveClick(View view) {
                                            LaunchHelper.getInstance().launch(mContext, VideoShowActivity.class, new VideoShowParcelable("", "", ""));
                                        }
                                    }
                            );
                        }
                    }else{
                        AlertDialog.showNoCanceled(getFragmentManager(), "", "视频聊天需要通过真人认证哦",
                                "去认证", "取消", new OnDialogClickListener() {
                                    @Override
                                    public void onNegativeClick(View view) {
                                    }

                                    @Override
                                    public void onPositiveClick(View view) {
                                        LaunchHelper.getInstance().launch(mContext, AuthenticationActivity.class);
                                    }
                                }
                        );
                    }

                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }

    private void goToWriteMessage() {
        if (mData.getUserBase() != null) {
            if (UserPreference.isAnchor()) {
                LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
                        mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));
            } else if (!UserPreference.isMale() && !UserPreference.isAnchor()) {
                getStatus();
            } else {
                LaunchHelper.getInstance().launch(mContext, ChatActivity.class, new ChatParcelable(mData.getUserBase().getGuid(),
                        mData.getUserBase().getAccount(), mData.getUserBase().getNickName(), mData.getUserBase().getIconUrlMininum(), 0));

            }
        }
    }

    @Subscribe
    public void onEvent(IsFollow follow) {
        if (follow.getAccount().equals(mData.getUserBase().getAccount())) {
            mData.setIsFollow(mData.getIsFollow() == 1 ? 0 : 1);
            if (mData.getIsFollow() == 1) {
                iv_follow.setVisibility(View.GONE);
            } else {
                iv_follow.setVisibility(View.VISIBLE);
            }
        }
    }

    @Subscribe
    public void onEvent(UpdataFollowUser follow) {
        if (follow.getGuid().equals(String.valueOf(mData.getUserBase().getGuid()))) {
            mData.setIsFollow(mData.getIsFollow() == 1 ? 0 : 1);
            if (mData.getIsFollow() == 1) {
                iv_follow.setVisibility(View.GONE);
            } else {
                iv_follow.setVisibility(View.VISIBLE);
            }
        }
    }

    @Subscribe
    public void onEvent(GiftSendEvent giftSendEvent) {
        if (getUserVisibleHint() &&  player_empty_room.isInPlayingState()
                && Util.isForeground(mContext,MainActivity.class.getName())) {
            goToWriteMessage();
        }
    }
    @Subscribe
    public void onEvent(StartEvent start) {
        if (getUserVisibleHint()) {
            GSYVideoManager.instance().onResume();
        }
    }

    @Subscribe
    public void onEvent(StartVideoEvent start) {
        if (getUserVisibleHint()&&getActivityPosition()==1) {
            if(player_empty_room!=null)
                player_empty_room.release();
            initViews();
            GSYVideoManager.instance().onResume();
        }
    }

    @Subscribe
    public void onEvent(PauseEvent pause) {
        if (player_empty_room != null) {
            GSYVideoManager.instance().onPause();
        }
    }
    //为点击事件埋点
    private void getClickNum(String remoteId,String extendTag,String baseTag){
        ApiManager.userActivityTag(UserPreference.getId(), remoteId, baseTag, extendTag, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {

            }

            @Override
            public void onError(String msg, boolean isNetworkError) {

            }
        });
    }
    public class getVideoCacheAsyncTask extends AsyncTask<String, Void, Bitmap> {
        private final Context context;
        private String imgUrl;

        public getVideoCacheAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            imgUrl =  params[0];
            //MediaMetadataRetriever 是android中定义好的一个类，提供了统一
            //的接口，用于从输入的媒体文件中取得帧和元数据；
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            try {
                //根据文件路径获取缩略图
                retriever.setDataSource(imgUrl, new HashMap());
                //获得第一帧图片
                return retriever.getFrameAtTime();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } finally {
                retriever.release();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result == null) {
                return;
            }
            String path = saveBitmap(mContext, result);
            SharedPreferenceUtil.setStringValue(context,"cacheImg",imgUrl,path);
            LogUtil.v("cacheImg","视频第一帧成功=="+path);


//            Bitmap bmp= BitmapFactory.decodeFile(path);
//            img.setImageBitmap(bmp);

        }
    }
    private static final String SD_PATH = "/sdcard/dskqxt/pic/";
    private static final String IN_PATH = "/dskqxt/pic/";

    /**
     * 随机生产文件名
     *
     * @return
     */
    private static String generateFileName() {
        return UUID.randomUUID().toString();
    }
    /**
     * 保存bitmap到本地
     *
     * @param context
     * @param mBitmap
     * @return
     */
    public static String saveBitmap(Context context, Bitmap mBitmap) {
        String savePath;
        File filePic;
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            savePath = SD_PATH;
        } else {
            savePath = context.getApplicationContext().getFilesDir()
                    .getAbsolutePath()
                    + IN_PATH;
        }
        try {
            filePic = new File(savePath + generateFileName() + ".jpg");
            if (!filePic.exists()) {
                filePic.getParentFile().mkdirs();
                filePic.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(filePic);
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

        return filePic.getPath();
    }
}
