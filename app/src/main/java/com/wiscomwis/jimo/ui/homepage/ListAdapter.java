package com.wiscomwis.jimo.ui.homepage;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscomwis.jimo.C;
import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.BaseModel;
import com.wiscomwis.jimo.data.model.SearchUser;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.library.adapter.CommonRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;

import java.util.List;

/**
 * 首页推荐用户适配器
 * Created by zhangdroid on 2017/6/3.
 */
public class ListAdapter extends CommonRecyclerViewAdapter<SearchUser> {
    private FragmentManager mFragmentManager;
    private Context mContext;
    private int pos = 0;

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.mFragmentManager = fragmentManager;
    }

    public ListAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
        mContext = context;
    }

    public ListAdapter(Context context, int layoutResId, List<SearchUser> dataList) {
        super(context, layoutResId, dataList);
    }

    @Override
    public void convert(final SearchUser searchUser, final int position, RecyclerViewHolder holder) {
        if (null != searchUser) {
            ImageView imageView = (ImageView) holder.getView(R.id.item_homepage_iv_avatar);
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(searchUser.getIconUrlMiddle())
                    .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(imageView).build());
            holder.setText(R.id.item_homepage_tv_nickname, searchUser.getNickName());
            TextView tv_big_state = (TextView) holder.getView(R.id.item_homepage_big_tv_state);
            final int status = searchUser.getStatus();
            int onlineStatus = searchUser.getOnlineStatus();
            if (onlineStatus == -1) {
                tv_big_state.setText(mContext.getString(R.string.not_online));
            } else {
                tv_big_state.setText(mContext.getString(R.string.edit_info_status));
            }
        }
    }

}
