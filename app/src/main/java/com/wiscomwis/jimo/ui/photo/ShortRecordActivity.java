package com.wiscomwis.jimo.ui.photo;

import android.Manifest;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.media.MediaMetadataRetriever;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.OrientationEventListener;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseFragmentActivity;
import com.wiscomwis.jimo.common.BitmapUtils;
import com.wiscomwis.library.net.NetUtil;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;

/**
 * Created by xuzhaole on 2018/3/1.
 */

public class ShortRecordActivity extends BaseFragmentActivity implements SurfaceHolder.Callback, View.OnClickListener {

    @BindView(R.id.surfaceview)
    SurfaceView cameraShowView;
    @BindView(R.id.iv_finish)
    ImageView iv_finish;
    @BindView(R.id.iv_swicth_camera)
    ImageView iv_swicth_camera;
    @BindView(R.id.tv_record_time)
    TextView tv_record_time;
    @BindView(R.id.iv_record_complete)
    ImageView iv_record_complete;
    @BindView(R.id.iv_record_start)
    ImageView iv_record_start;

    MediaRecorder recorder;
    SurfaceHolder surfaceHolder;

    Camera camera;

    OrientationEventListener orientationEventListener;

    File videoFile;

    int rotationRecord = 90;

    int rotationFlag = 90;


    int cameraType = 1;

    int cameraFlag = 0; //1为后置

    boolean flagRecord = false;//是否正在录像

    private int time = 0;

    Handler handler = new Handler();

    private String getTime(int time) {
        String getTime = "00:00";
        if (time < 10) {
            getTime = "00:0" + time;
        } else if (time < 60) {
            getTime = "00:" + time;
        }
        return getTime;
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            time++;
            if (time >= 10) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        handler.removeCallbacks(this);
                        endRecord();
                    }
                });
            }
            tv_record_time.setText(getTime(time));
            handler.postDelayed(this, 1000);
        }
    };

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceHolder = holder;
        initCamera(cameraType);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int i, int i1, int i2) {
        surfaceHolder = holder;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        endRecord();
        releaseCamera();
    }

    /**
     * 开始录制时候的状态
     */
    private void startRecordUI() {
        iv_swicth_camera.setVisibility(View.GONE); // 旋转摄像头关闭
        iv_finish.setVisibility(View.GONE);
        iv_record_complete.setVisibility(View.VISIBLE);
        iv_record_start.setVisibility(View.GONE);
    }

    /**
     * 旋转界面UI
     */
    private void rotationUIListener() {
        orientationEventListener = new OrientationEventListener(this) {
            @Override
            public void onOrientationChanged(int rotation) {
                if (!flagRecord) {
                    if (((rotation >= 0) && (rotation <= 30)) || (rotation >= 330)) {
                        // 竖屏拍摄
                        if (rotationFlag != 0) {
                            //旋转logo
                            rotationAnimation(rotationFlag, 0);
                            //这是竖屏视频需要的角度
                            rotationRecord = 90;
                            //这是记录当前角度的flag
                            rotationFlag = 0;
                        }
                    } else if (((rotation >= 230) && (rotation <= 310))) {
                        // 横屏拍摄
                        if (rotationFlag != 90) {
                            //旋转logo
                            rotationAnimation(rotationFlag, 90);
                            //这是正横屏视频需要的角度
                            rotationRecord = 0;
                            //这是记录当前角度的flag
                            rotationFlag = 90;
                        }
                    } else if (rotation > 30 && rotation < 95) {
                        // 反横屏拍摄
                        if (rotationFlag != 270) {
                            //旋转logo
                            rotationAnimation(rotationFlag, 270);
                            //这是反横屏视频需要的角度
                            rotationRecord = 180;
                            //这是记录当前角度的flag
                            rotationFlag = 270;
                        }
                    }
                }
            }
        };
        orientationEventListener.enable();
    }

    private void rotationAnimation(int from, int to) {
        ValueAnimator progressAnimator = ValueAnimator.ofInt(from, to);
        progressAnimator.setDuration(300);
        progressAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int currentAngle = (int) animation.getAnimatedValue();
                // videoTime.setRotation(currentAngle);
                iv_swicth_camera.setRotation(currentAngle);
            }
        });
        progressAnimator.start();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_swicth_camera:
                switchCamera();
                break;
            case R.id.iv_record_start://开始录制视频
                startRecordUI();
                startRecord();
                handler.postDelayed(runnable, 1000);
                break;
            case R.id.iv_record_complete://录制完成，返回
                handler.removeCallbacks(runnable);
                endRecord();
                break;
            case R.id.iv_finish:
                finish();
                break;
        }
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_short_record;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return false;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            Toast.makeText(this, "SD card", Toast.LENGTH_SHORT).show();
            finish();
        }

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory
                (Environment.DIRECTORY_DCIM), "MyCameraApp");
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        videoFile = new File(mediaStorageDir.getPath() + File.separator + "VID_" + timeStamp + ".mp4");
        initView();
    }

    private void initView() {
        doStartSize();
        SurfaceHolder holder = cameraShowView.getHolder();
        holder.addCallback(this);
        // setType必须设置，要不出错.
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        rotationUIListener();
    }


    @Override
    protected void setListeners() {
        iv_swicth_camera.setOnClickListener(this);
        iv_finish.setOnClickListener(this);
        iv_record_start.setOnClickListener(this);
        iv_record_complete.setOnClickListener(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }


    /**
     * 初始化相机
     *
     * @param type 前后的类型
     */
    private void initCamera(int type) {

        if (camera != null) {
            //如果已经初始化过，就先释放
            releaseCamera();
        }

        try {
            camera = Camera.open(type);
            if (camera == null) {
                return;
            }
            camera.lock();
            Camera.Parameters parameters = camera.getParameters();
            if (type == 0) {
                //基本是都支持这个比例
                parameters.setPreviewSize(640, 480);
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);//1连续对焦
                camera.cancelAutoFocus();// 2如果要实现连续的自动对焦，这一句必须加上
            }
            camera.setParameters(parameters);
//            if (cameraType == 1) {
//                frontCameraRotate();
            camera.setDisplayOrientation(90);
//            } else {
//                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
//                    camera.setDisplayOrientation(90);//90
//                } else {
//                    camera.setDisplayOrientation(90);
//                }
//            }
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
            camera.unlock();
        } catch (Exception e) {
            e.printStackTrace();
            releaseCamera();
        }
    }

    private boolean startRecord() {

        //懒人模式，根据闪光灯和摄像头前后重新初始化一遍，开期闪光灯工作模式
        initCamera(cameraType);

        if (recorder == null) {
            recorder = new MediaRecorder();
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || camera == null || recorder == null) {
            camera = null;
            recorder = null;
            return false;
        }
        try {
            recorder.setCamera(camera);
            // 这两项需要放在setOutputFormat之前
            recorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
            recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
            // Set output file format，输出格式
            recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);

            //必须在setEncoder之前
//            recorder.setVideoFrameRate(15);  //帧数  一分钟帧，15帧就够了
            recorder.setVideoSize(640, 480);//这个大小就够了

            // 这两项需要放在setOutputFormat之后
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            recorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);

            recorder.setVideoEncodingBitRate(3 * 640 * 480);//第一个数字越大，清晰度就越高，考虑文件大小的缘故，就调整为1
            recorder.setOrientationHint((cameraType == 1) ? 270 : 90);
            //把摄像头的画面给它
            recorder.setPreviewDisplay(surfaceHolder.getSurface());
            //创建好视频文件用来保存
            //设置创建好的输入路径
            recorder.setOutputFile(videoFile.getPath());
            recorder.prepare();
            recorder.start();
            //不能旋转啦
            orientationEventListener.disable();
            flagRecord = true;
        } catch (Exception e) {
            //一般没有录制权限或者录制参数出现问题都走这里
            e.printStackTrace();
            //还是没权限啊
            recorder.reset();
            recorder.release();
            recorder = null;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (!TextUtils.isEmpty(videoFile.getPath())) {
                        File file = new File(videoFile.getPath());
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                }
            }).start();
            return false;
        }
        return true;
    }

    private void endRecord() {
        //反正多次进入，比如surface的destroy和界面onPause
        if (!flagRecord) {
            return;
        }
        flagRecord = false;
        try {
            if (recorder != null) {
                recorder.stop();
                recorder.reset();
                recorder.release();
                orientationEventListener.enable();
                recorder = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(videoFile.getAbsolutePath());
        final Bitmap bitmap = mmr.getFrameAtTime();
        mmr.release();

        new AsyncTask<Void, Void, File>() {
            @Override
            protected File doInBackground(Void... voids) {
                return BitmapUtils.compressImage(ShortRecordActivity.this, bitmap);
            }

            @Override
            protected void onPostExecute(File file) {
                Intent intent = new Intent(ShortRecordActivity.this, GetVideoActivity.class);
                //将结果返回上个界面
                intent.putExtra("bitmapFilePath", file.getAbsolutePath());
                intent.putExtra("duration", time + "");
                intent.putExtra("videoFilePath", videoFile.getAbsolutePath());
                setResult(RESULT_OK, intent);
                finish();
            }
        }.execute();

    }

    /**
     * 释放摄像头资源
     */
    private void releaseCamera() {
        try {
            if (camera != null) {
                camera.setPreviewCallback(null);
                camera.stopPreview();
                camera.lock();
                camera.release();
                camera = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 切换摄像头
     */
    public void switchCamera() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraCount = Camera.getNumberOfCameras();//得到摄像头的个数0或者1;

        try {
            for (int i = 0; i < cameraCount; i++) {
                Camera.getCameraInfo(i, cameraInfo);//得到每一个摄像头的信息
                if (cameraFlag == 1) {
                    //后置到前置
                    if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {//代表摄像头的方位，CAMERA_FACING_FRONT前置      CAMERA_FACING_BACK后置
                        frontCameraRotate();//前置旋转摄像头度数
                        switchCameraLogic(i, 0);
                        break;
                    }
                } else {
                    //前置到后置
                    if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {//代表摄像头的方位，CAMERA_FACING_FRONT前置      CAMERA_FACING_BACK后置
                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
                            switchCameraLogic(i, 1);
                        } else {
                            switchCameraLogic(i, 1);
                        }
                        break;
                    }
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /***
     * 处理摄像头切换逻辑
     *
     * @param i           哪一个，前置还是后置
     * @param flag        切换后的标志
     */
    private void switchCameraLogic(int i, int flag) {
        if (camera != null) {
            camera.lock();
        }
        releaseCamera();
        camera = Camera.open(i);//打开当前选中的摄像头
        try {
            camera.setDisplayOrientation(90);
            camera.setPreviewDisplay(surfaceHolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        cameraFlag = flag;
        camera.startPreview();
        cameraType = i;
        camera.unlock();
    }

    /**
     * 旋转前置摄像头为正的
     */
    private void frontCameraRotate() {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(1, info);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        finish();
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 因为录制改分辨率的比例可能和屏幕比例一直，所以需要调整比例显示
     */
    private void doStartSize() {
        int screenWidth = getScreenWidth();
        int screenHeight = getScreenHeight();
        setViewSize(cameraShowView, screenWidth * 640 / 480, screenHeight);
    }

    private int getScreenHeight() {
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    private int getScreenWidth() {
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static void setViewSize(View view, int width, int height) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (null == layoutParams)
            return;
        layoutParams.width = width;
        layoutParams.height = height;
        view.setLayoutParams(layoutParams);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (flagRecord) {
                endRecord();
                if (camera != null && cameraType == 0) {
                    //关闭后置摄像头闪光灯
                    camera.lock();
                    camera.unlock();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (flagRecord) {
            //如果是录制中的就完成录制
            onPause();
            return;
        }
        super.onBackPressed();
    }
}
