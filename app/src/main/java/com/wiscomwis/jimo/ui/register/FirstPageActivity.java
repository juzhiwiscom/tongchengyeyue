package com.wiscomwis.jimo.ui.register;

import android.os.Parcelable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseFragmentActivity;
import com.wiscomwis.jimo.base.CommonWebActivity;
import com.wiscomwis.jimo.data.api.ApiConstant;
import com.wiscomwis.jimo.event.RegisterAndLoginFinish;
import com.wiscomwis.jimo.parcelable.WebParcelable;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.util.LaunchHelper;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

/**
 * Created by Administrator on 2017/8/26.
 */

public class FirstPageActivity extends BaseFragmentActivity implements View.OnClickListener {
    @BindView(R.id.scrollview)
    HorizontalScrollView scrollview;
    @BindView(R.id.firstpage_activity_iv_bg)
    ImageView iv_bg;
    @BindView(R.id.register)
    Button btn_register;
    @BindView(R.id.iv_male)
    ImageView iv_male;
    @BindView(R.id.rl_male)
    RelativeLayout rl_male;
    @BindView(R.id.iv_female)
    ImageView iv_female;
    @BindView(R.id.rl_female)
    RelativeLayout rl_female;
    @BindView(R.id.register_xieyi)
    TextView tv_xieyi;
    @BindView(R.id.register_weixin)
    Button register_weixin;
    private boolean isMale = true;
//    private String mNickName;
    private IWXAPI wxAPI;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_firstpage;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return true;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return null;
    }

    @Override
    protected void initViews() {
        wxAPI = WXAPIFactory.createWXAPI(this,"wx557a81556f6683a0",true);
        wxAPI.registerApp("wx557a81556f6683a0");

        scrollview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        setAnimation();
        String text = tv_xieyi.getText().toString();
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                LaunchHelper.getInstance().launch(FirstPageActivity.this, CommonWebActivity.class,
                        new WebParcelable(getString(R.string.user_agreement), ApiConstant.URL_AGREEMENT_USER, false));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.main_color));
            }

        }, text.indexOf("《"), text.indexOf("》") + 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                LaunchHelper.getInstance().launch(FirstPageActivity.this, CommonWebActivity.class,
                        new WebParcelable(getString(R.string.privacy_agreement), ApiConstant.URL_AGREEMENT_PRIVACY, false));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.main_color));
            }
        }, text.lastIndexOf("《"), text.lastIndexOf("》") + 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        tv_xieyi.setMovementMethod(new LinkMovementMethod());
        tv_xieyi.setText(spannableStringBuilder);
    }

    @Override
    protected void setListeners() {
        btn_register.setOnClickListener(this);
        register_weixin.setOnClickListener(this);
        rl_male.setOnClickListener(this);
        rl_female.setOnClickListener(this);
    }

    @Override
    protected void loadData() {
//        btn_register.getBackground().setAlpha(200);
//        ApiManager.nickName(new IGetDataListener<NickName>() {
//
//            @Override
//            public void onResult(NickName nickName, boolean isEmpty) {
//                if (nickName != null) {
//                    mNickName = nickName.getNickName();
//                }
//            }
//
//            @Override
//            public void onError(String msg, boolean isNetworkError) {
//
//            }
//        });

    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    private void setAnimation() {
        TranslateAnimation animation = new TranslateAnimation(-400f, -900f, 0.0f, 0.0f);
        animation.setDuration(20000);
        animation.setRepeatMode(TranslateAnimation.REVERSE);
        animation.setInterpolator(new LinearInterpolator());
        //设置动画播放次数
        animation.setRepeatCount(TranslateAnimation.INFINITE);
        //启动动画
        iv_bg.startAnimation(animation);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_male://男
                if (!isMale) {
                    rl_male.setBackgroundResource(R.drawable.checked_first_page);
                    rl_female.setBackgroundResource(R.drawable.uncheck_first_page);
                    isMale = true;
                }
                break;
            case R.id.rl_female://女
                if (isMale) {
                    rl_female.setBackgroundResource(R.drawable.checked_first_page);
                    rl_male.setBackgroundResource(R.drawable.uncheck_first_page);
                    isMale = false;
                }
                break;
            case R.id.register://开启泡泡之旅

//                LoadingDialog.show(getSupportFragmentManager());
//                if (!TextUtils.isEmpty(mNickName)) {
//                    register();
//                } else {
//                    ApiManager.nickName(new IGetDataListener<NickName>() {
//
//                        @Override
//                        public void onResult(NickName nickName, boolean isEmpty) {
//                            if (nickName != null) {
//                                mNickName = nickName.getNickName();
//                                register();
//                            }
//                        }
//
//                        @Override
//                        public void onError(String msg, boolean isNetworkError) {
//                            LoadingDialog.hide();
//                            ToastUtil.showShortToast(mContext, TextUtils.isEmpty(msg) ? getString(R.string.tip_error) : msg);
//                        }
//                    });
//                }

                break;
            case R.id.register_weixin:
                SendAuth.Req req = new SendAuth.Req();
                req.scope = "snsapi_userinfo";
                req.state = String.valueOf(System.currentTimeMillis());
                wxAPI.sendReq(req);
                break;
        }
    }

//    private void register() {
//        ApiManager.register(mNickName, isMale, "0",
//                new IGetDataListener<Register>() {
//                    @Override
//                    public void onResult(Register register, boolean isEmpty) {
//                        LoadingDialog.hide();
//                        // 保存用户信息到本地
//                        UserBase userBase = register.getUserBase();
//                        if (null != userBase) {
//                            UserPreference.saveUserInfo(userBase);
//                        }
//                        DbModle.getInstance().getUserAccountDao().deleteAllData();//删除数据库中的所有数据
//                        // 关闭之前打开的页面
//                        EventBus.getDefault().post(new FinishEvent());
//                        // 跳转主页面
//                        LaunchHelper.getInstance().launchFinish(mContext, CompleteInfoActivity.class);
//                        UserPreference.registered();//注册成功的标记
//                    }
//
//                    @Override
//                    public void onError(String msg, boolean isNetworkError) {
//                        LoadingDialog.hide();
//                        if (isNetworkError) {
//                            ToastUtil.showShortToast(FirstPageActivity.this, getString(R.string.tip_network_error));
//                        } else {
//                            ToastUtil.showShortToast(FirstPageActivity.this, TextUtils.isEmpty(msg) ? getString(R.string.tip_empty) : msg);
//                        }
//                    }
//                });
//    }

    @Subscribe
    public void onEvent(RegisterAndLoginFinish event) {
        finish();
    }

}
