package com.wiscomwis.jimo.ui.follow;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.base.BaseFragmentActivity;
import com.wiscomwis.jimo.event.FollowEvent;
import com.wiscomwis.jimo.event.IsFollow;
import com.wiscomwis.jimo.event.UpdataFollowUser;
import com.wiscomwis.jimo.ui.follow.contract.FollowContract;
import com.wiscomwis.jimo.ui.follow.presenter.FollowPresenter;
import com.wiscomwis.library.adapter.wrapper.OnLoadMoreListener;
import com.wiscomwis.library.net.NetUtil;
import com.wiscomwis.library.widget.AutoSwipeRefreshLayout;
import com.wiscomwis.library.widget.XRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 关注
 * Created by zhangdroid on 2017/5/23.
 */
public class FollowActivity extends BaseFragmentActivity implements FollowContract.IView {
    @BindView(R.id.fragment_follow_xrecyerview)
    XRecyclerView mRecyclerView;
    @BindView(R.id.fragment_follow_swiperefresh)
    AutoSwipeRefreshLayout mSwipeRefresh;
    @BindView(R.id.fragment_follow_ll)
    LinearLayout ll_follow;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    private FollowPresenter mFollowPresenter;
    private LinearLayoutManager linearLayoutManager;
    // 初次进入时自动显示刷新，此时不调用刷新
    private boolean mIsFirstLoad;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_follow;
    }

    @Override
    protected boolean isApplyTranslucentStatusBar() {
        return false;
    }

    @Override
    protected boolean isRegistEventBus() {
        return true;
    }

    @Override
    protected void getBundleExtras(Parcelable parcelable) {

    }

    @Override
    protected View getNoticeView() {
        return ll_follow;
    }


    @Override
    protected void initViews() {
        mSwipeRefresh.setColorSchemeResources(R.color.main_color);
        mSwipeRefresh.setProgressBackgroundColorSchemeColor(Color.WHITE);
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mFollowPresenter = new FollowPresenter(this);
        mIsFirstLoad = true;
    }

    @Override
    protected void setListeners() {
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mFollowPresenter.refresh();
            }
        });
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void loadData() {
        mFollowPresenter.loadFollowUserList();
    }

    @Override
    protected void networkConnected(NetUtil.NetType type) {

    }

    @Override
    protected void networkDisconnected() {

    }

    @Override
    public Context obtainContext() {
        return mContext;
    }

    @Override
    public void showTip(String msg) {

    }


    @Override
    public void hideRefresh(int delaySeconds) {
        mSwipeRefresh.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mSwipeRefresh.isRefreshing()) {
                    mSwipeRefresh.setRefreshing(false);
                }
            }
        }, delaySeconds * 1000);
    }

    @Override
    public void toggleShowError(boolean toggle, String msg) {
        super.toggleShowError(toggle, msg, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowError(false, null, null);
                mFollowPresenter.refresh();
            }
        });
    }

    @Override
    public void toggleShowEmpty(boolean toggle, String msg) {
        super.toggleShowEmpty(toggle, mContext.getString(R.string.follow_empty), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //男的跳到热门页，女的跳到发现页(在热门页做了一个判断)
                EventBus.getDefault().post(new FollowEvent());
            }
        });
    }

    @Override
    public FragmentManager obtainFragmentManager() {
        return getSupportFragmentManager();
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter, int loadMoreViewId) {
        mRecyclerView.setAdapter(adapter, loadMoreViewId);
    }

    @Override
    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        mRecyclerView.setOnLoadingMoreListener(onLoadMoreListener);
    }

    @Override
    public void showLoadMore() {
        mRecyclerView.setVisibility(R.id.load_more_progress, true);
        mRecyclerView.setVisibility(R.id.load_more_msg, true);
        mRecyclerView.setVisibility(R.id.load_more_empty, false);
        mRecyclerView.startLoadMore();
    }

    @Override
    public void hideLoadMore() {
        mRecyclerView.finishLoadMore();
    }

    @Override
    public void showNoMore() {
        mRecyclerView.setVisibility(R.id.load_more_empty, true);
        mRecyclerView.setVisibility(R.id.load_more_progress, false);
        mRecyclerView.setVisibility(R.id.load_more_msg, false);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Subscribe
    public void onEvent(UpdataFollowUser updataFollowUser) {
        handler.sendEmptyMessage(1);
    }

    @Subscribe
    public void onEvent(IsFollow follow) {
        mFollowPresenter.refresh();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Toast.makeText(mContext, mContext.getString(R.string.user_detail_follow_cancle), Toast.LENGTH_SHORT).show();
            mFollowPresenter.refresh();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
