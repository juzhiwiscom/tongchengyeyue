package com.wiscomwis.jimo.ui.charmandrankinglist.presenter;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.common.CustomDialogAboutPay;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.VideoSquare;
import com.wiscomwis.jimo.data.model.VideoSquareList;
import com.wiscomwis.jimo.event.StartEvent;
import com.wiscomwis.jimo.event.StartVideoEvent;
import com.wiscomwis.jimo.ui.charmandrankinglist.adapter.VideoListAdapter;
import com.wiscomwis.jimo.ui.charmandrankinglist.contract.VideoListContract;
import com.wiscomwis.library.util.LogUtil;
import com.wiscomwis.library.util.SharedPreferenceUtil;
import com.wiscomwis.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by xuzhaole on 2018/3/20.
 */

public class VideoListPresenter implements VideoListContract.IPresenter {
    private static final String SD_PATH = "/sdcard/dskqxt/pic/";
    private static final String IN_PATH = "/dskqxt/pic/";
    public VideoSquareList videoList;
    private VideoListAdapter adapter;
    private VideoListContract.IView mVideoListView;
    private Context mContext;
    private boolean isRefish = false;

    public VideoListPresenter(VideoListContract.IView videoListView) {
        this.mVideoListView = videoListView;
        mContext = videoListView.obtainContext();
    }

    /**
     * 保存bitmap到本地
     *
     * @param context
     * @param mBitmap
     * @return
     */
    public static String saveBitmap(Context context, Bitmap mBitmap) {
        String savePath;
        File filePic;
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            savePath = SD_PATH;
        } else {
            savePath = context.getApplicationContext().getFilesDir()
                    .getAbsolutePath()
                    + IN_PATH;
        }
        try {
            filePic = new File(savePath + generateFileName() + ".jpg");
            if (!filePic.exists()) {
                filePic.getParentFile().mkdirs();
                filePic.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(filePic);
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        return filePic.getPath();
    }

    /**
     * 随机生产文件名
     *
     * @return
     */
    private static String generateFileName() {
        return UUID.randomUUID().toString();
    }

    @Override
    public void start() {
        adapter = new VideoListAdapter(((FragmentActivity) mContext).getSupportFragmentManager());
        mVideoListView.setAdapter(adapter);
    }

    @Override
    public void loadVideoShowList(int num) {
        if (num == 100) {
            num = 1;
            isRefish = true;
            mVideoListView.clearPosition();
        }
        final int finalNum = num;
        final int finalNum1 = num;

        ApiManager.VideoShowList(num + "", "5", new IGetDataListener<VideoSquareList>() {
            @Override
            public void onResult(VideoSquareList videoSquareList, boolean isEmpty) {
                videoList = videoSquareList;
                if (videoSquareList.getVideoSquareList() == null || videoSquareList.getVideoSquareList().size() == 0) {
                    if (finalNum == 1) {
                        mVideoListView.setEmptyView(true, null);
                    } else {
                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.tip_no_more));
                    }
                } else {
//                    将图片缓存
                    cachePhoto(videoSquareList.getVideoSquareList(), finalNum1);
                    mVideoListView.setPostion();
                    if (finalNum == 1) {
                        if (isRefish) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    EventBus.getDefault().post(new StartVideoEvent());
                                }
                            }, 500);
                            isRefish = false;
                            start();
                        }
                        adapter.setData(videoSquareList.getVideoSquareList());

                    } else {
                        adapter.addData(videoSquareList.getVideoSquareList());
                    }
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mVideoListView.setErrorView(true, msg);
            }
        });
    }

    @Override
    public void reportShow(int currentPosition) {
        List<VideoSquare> data = adapter.getData();
        if (data != null && currentPosition < data.size()) {
            VideoSquare videoSquare = data.get(currentPosition);
            long guid = videoSquare.getUserBase().getGuid();
            CustomDialogAboutPay.reportShow(mContext, guid + "");
        }
    }

    private void cachePhoto(List<VideoSquare> videoSquareList, int finalNum1) {
        for (int i = 0; i < videoSquareList.size(); i++) {
            String videoUrl = videoSquareList.get(i).gettUserVideoShow().getVideoUrl();
            if (!TextUtils.isEmpty(videoUrl)
                    && SharedPreferenceUtil.getStringValue(mContext, "cacheImg", videoUrl, "url").equals("url")) {
                new getVideoCacheAsyncTask(mContext).execute(videoUrl);
            }
            if (i == videoSquareList.size() - 1 && finalNum1 != 0) {
//                继续加载数据
                uploadPhoto(finalNum1 + 1);
            }
        }
    }

    private void uploadPhoto(final int finalNum1) {
        ApiManager.VideoShowList(finalNum1 + 1 + "", "5", new IGetDataListener<VideoSquareList>() {
            @Override
            public void onResult(VideoSquareList videoSquareList, boolean isEmpty) {
//                videoList=videoSquareList;
                if (videoSquareList.getVideoSquareList() == null || videoSquareList.getVideoSquareList().size() == 0) {
                    if (finalNum1 == 1) {
//                        mVideoListView.setEmptyView(true, null);
                    } else {
//                        ToastUtil.showShortToast(mContext, mContext.getString(R.string.tip_no_more));
                    }
                } else {
//                    将图片缓存
                    cachePhoto(videoSquareList.getVideoSquareList(), 0);
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mVideoListView.setErrorView(true, msg);
            }
        });
    }

    public class getVideoCacheAsyncTask extends AsyncTask<String, Void, Bitmap> {
        private final Context context;
        private String imgUrl;

        public getVideoCacheAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            imgUrl = params[0];
            //MediaMetadataRetriever 是android中定义好的一个类，提供了统一
            //的接口，用于从输入的媒体文件中取得帧和元数据；
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
            try {
                //根据文件路径获取缩略图
                retriever.setDataSource(imgUrl, new HashMap());
                //获得第一帧图片
                return retriever.getFrameAtTime();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                retriever.release();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result == null) {
                return;
            }
            String path = saveBitmap(mContext, result);
            SharedPreferenceUtil.setStringValue(context, "cacheImg", imgUrl, path);
            LogUtil.v("cacheImg", "视频第一帧成功==" + path);
        }
    }
}
