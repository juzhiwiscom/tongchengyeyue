package com.wiscomwis.jimo.ui.homepage.adapter;

import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.data.model.SearchUser;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.library.adapter.base.BaseQuickAdapter;
import com.wiscomwis.library.adapter.base.BaseViewHolder;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;


/**
 * Created by xuzhaole on 2018/1/9.
 */

public class HomeListAdapter extends BaseQuickAdapter<SearchUser, BaseViewHolder> {

    public HomeListAdapter(@LayoutRes int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(final BaseViewHolder holder, final SearchUser searchUser) {
        if (searchUser != null) {
            ImageView imageView = holder.getView(R.id.item_homepage_iv_avatar);
            ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(searchUser.getIconUrlMiddle())
                    .placeHolder(Util. getDefaultImage()).error(Util.getDefaultImage()).imageView(imageView).build());
            holder.setText(R.id.item_homepage_tv_nickname, searchUser.getNickName());
            TextView tv_age = holder.getView(R.id.item_homepage_tv_age);
            TextView tv_online = holder.getView(R.id.item_homepage_big_tv_online);
            RelativeLayout rl_sex = holder.getView(R.id.item_homepage_rl_age_sex);
            TextView tv_big_state = holder.getView(R.id.item_homepage_big_tv_state);
            // 视频

            if(UserPreference.isMale()){
                rl_sex.setBackgroundResource(R.drawable.icon_famle);
            }else{
                rl_sex.setBackgroundResource(R.drawable.icon_male);
            }
            tv_age.setText(searchUser.getAge());
            int onlineStatus = searchUser.getOnlineStatus();
            if (onlineStatus == 3) {
                tv_online.setBackgroundResource(R.drawable.linear_circle_red_bg);
                tv_online.setVisibility(View.VISIBLE);
                tv_big_state.setText("通话中...");
            } else if(onlineStatus==2){
                tv_online.setBackgroundResource(R.drawable.linear_circle_green_bg);
                tv_online.setVisibility(View.VISIBLE);
                tv_big_state.setText("私聊");
            }else{
                tv_online.setBackgroundResource(R.drawable.linear_circle_green_bg);
                tv_online.setVisibility(View.VISIBLE);
                tv_big_state.setText("空闲");
            }
        }
    }
}
