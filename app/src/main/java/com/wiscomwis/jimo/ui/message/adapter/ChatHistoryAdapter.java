package com.wiscomwis.jimo.ui.message.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.common.TimeUtils;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.data.model.HuanXinUser;
import com.wiscomwis.jimo.parcelable.UserDetailParcelable;
import com.wiscomwis.jimo.ui.detail.UserDetailActivity;
import com.wiscomwis.library.adapter.CommonRecyclerViewAdapter;
import com.wiscomwis.library.adapter.RecyclerViewHolder;
import com.wiscomwis.library.image.ImageLoader;
import com.wiscomwis.library.image.ImageLoaderUtil;
import com.wiscomwis.library.util.LaunchHelper;

/**
 * 聊天记录适配器
 * Created by zhangdroid on 2017/7/6.
 */
public class ChatHistoryAdapter extends CommonRecyclerViewAdapter<HuanXinUser> {

    public ChatHistoryAdapter(Context context, int layoutResId) {
        super(context, layoutResId);
    }

    @Override
    public void convert(final HuanXinUser user, int position, RecyclerViewHolder holder) {
        if (user != null) {
            String hxId = user.getHxId();
            if (!TextUtils.isEmpty(hxId)) {
                ImageView ivAvatar = (ImageView) holder.getView(R.id.item_chat_history_avatar);
                RelativeLayout view = (RelativeLayout) holder.getView(R.id.item_chat_history_rl_view);
                if (user.getHxId().equals("10000")) {
                    view.setVisibility(View.GONE);
                } else {
                    view.setVisibility(View.VISIBLE);
                }
                ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(user.getHxIcon())
                        .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(ivAvatar).build());
                holder.setText(R.id.item_chat_history_nickname, user.getHxName());
                holder.setText(R.id.item_chat_history_message, String.valueOf(user.getLastMsg()));
                TextView tv_msgNum = (TextView) holder.getView(R.id.item_chat_history_tv_msgnum);
                if (user.getMsgNum() > 0) {
                    tv_msgNum.setVisibility(View.VISIBLE);
                    tv_msgNum.setText(user.getMsgNum() + "");
                } else {
                    tv_msgNum.setVisibility(View.GONE);
                }
//
                holder.setText(R.id.item_chat_is_tv_time, TimeUtils.getLocalTime(mContext, System.currentTimeMillis(), Long.parseLong(user.getMsgTime())));
                ivAvatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
                                new UserDetailParcelable(user.getHxId()));
                    }
                });
            } else {
            }
        }

    }


}

// if(list!=null&&list.size()>0){
//final HuanXinUser user = list.get(position);
//        if (user!=null) {
//        String hxId = user.getHxId();
//        if (!TextUtils.isEmpty(hxId)) {
//        ImageView ivAvatar = (ImageView) holder.getView(R.id.item_chat_history_avatar);
//        ImageLoaderUtil.getInstance().loadImage(mContext, new ImageLoader.Builder().url(user.getHxIcon())
//        .placeHolder(Util.getDefaultImage()).error(Util.getDefaultImage()).imageView(ivAvatar).build());
//        holder.setText(R.id.item_chat_history_nickname,user.getHxName());
//        holder.setText(R.id.item_chat_history_message,user.getLastMsg());
//        TextView tv_msgNum = (TextView) holder.getView(R.id.item_chat_history_tv_msgnum);
//        if(!TextUtils.isEmpty(user.getMsgNum())&&user.getMsgNum().length()>0){
//        int i = Integer.parseInt(user.getMsgNum());
//        if(i>0){
//        tv_msgNum.setVisibility(View.VISIBLE);
//        tv_msgNum.setText(user.getMsgNum());
//        }else{
//        tv_msgNum.setVisibility(View.GONE);
//        }
//        }
//        holder.setText(R.id.item_chat_is_tv_time,  DateTimeUtil.convertTimeMillis2String(Long.parseLong(user.getMsgTime())));
//        ivAvatar.setOnClickListener(new View.OnClickListener() {
//@Override
//public void onClick(View v) {
//        LaunchHelper.getInstance().launch(mContext, UserDetailActivity.class,
//        new UserDetailParcelable(user.getHxId()));
//        }
//        });
//        }else{
//        FrameLayout view = (FrameLayout) holder.getView(R.id.item_chat_history_card_view);
//        view.setVisibility(View.GONE);
//        }
//        }
//        }