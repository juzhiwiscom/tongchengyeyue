package com.wiscomwis.jimo.ui.detail.presenter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.wiscomwis.jimo.R;
import com.wiscomwis.jimo.common.Util;
import com.wiscomwis.jimo.data.api.ApiManager;
import com.wiscomwis.jimo.data.api.IGetDataListener;
import com.wiscomwis.jimo.data.model.BaseModel;
import com.wiscomwis.jimo.data.model.MyInfo;
import com.wiscomwis.jimo.data.model.TUserVideoShow;
import com.wiscomwis.jimo.data.model.UpLoadMyPhoto;
import com.wiscomwis.jimo.data.model.UserBase;
import com.wiscomwis.jimo.data.model.UserDetail;
import com.wiscomwis.jimo.data.model.UserPhoto;
import com.wiscomwis.jimo.data.preference.UserPreference;
import com.wiscomwis.jimo.event.RecordImageEvent;
import com.wiscomwis.jimo.ui.detail.adapter.NewAlbumAdapter;
import com.wiscomwis.jimo.ui.detail.contract.AlbumContract;
import com.wiscomwis.jimo.ui.photo.GetPhotoActivity;
import com.wiscomwis.jimo.ui.photo.GetVideoActivity;
import com.wiscomwis.library.adapter.base.BaseQuickAdapter;
import com.wiscomwis.library.adapter.base.callback.ItemDragAndSwipeCallback;
import com.wiscomwis.library.adapter.base.listener.OnItemDragListener;
import com.wiscomwis.library.dialog.LoadingDialog;
import com.wiscomwis.library.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.List;

/**
 * Created by Administrator on 2017/6/14.
 */
public class AlbumPresenter implements AlbumContract.IPresenter {
    private AlbumContract.IView mAlbumIview;
    private Context mContext;
    private NewAlbumAdapter mAlbumPhotoAdapter;
    private ImageView item_photo_iv;
    private TextView tv_video_time;
    private String videoShowId;
    private List<UserPhoto> mUserPhotos;
    private UserPhoto fromUserPhoto;
    private UserPhoto toUserPhoto;
    private int fromPosition;

    public AlbumPresenter(AlbumContract.IView mAlbumIview) {
        this.mAlbumIview = mAlbumIview;
        this.mContext = mAlbumIview.obtainContext();
    }

    @Override
    public void start() {
    }

    @Override
    public void start(RecyclerView recyclerView) {
        mAlbumPhotoAdapter = new NewAlbumAdapter(null);


        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.album_add_item, null);
        item_photo_iv = (ImageView) view.findViewById(R.id.iv_record);
        tv_video_time = (TextView) view.findViewById(R.id.tv_video_time);
        TextView tv_desc_3 = (TextView) view.findViewById(R.id.tv_desc_3);
        if (UserPreference.isMale()) {
            tv_desc_3.setText(mContext.getString(R.string.album_desc_3_male));
        } else {
            tv_desc_3.setText(mContext.getString(R.string.album_desc_3_female));
        }
        mAlbumPhotoAdapter.setHeaderView(view);
        item_photo_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetVideoActivity.toGetVideoActivity(mContext, true, new GetVideoActivity.OnGetVideoListener() {
                    @Override
                    public void getSelectedVideo(String filePath, String duration, String bitmapFilePath) {
                        uploadVideoShow(!TextUtils.isEmpty(videoShowId) ? videoShowId : "0", new File(bitmapFilePath), new File(filePath), duration);
                    }
                });
            }
        });

        OnItemDragListener onItemDragListener = new OnItemDragListener() {
            @Override
            public void onItemDragStart(RecyclerView.ViewHolder viewHolder, int pos) {
                fromUserPhoto = mAlbumPhotoAdapter.getItem(pos);
                fromPosition = pos;
            }

            @Override
            public void onItemDragMoving(RecyclerView.ViewHolder source, int from, RecyclerView.ViewHolder target, int to) {
            }

            @Override
            public void onItemDragEnd(RecyclerView.ViewHolder viewHolder, int pos) {
                if (pos > fromPosition) {//从小往大移动
                    toUserPhoto = mAlbumPhotoAdapter.getItem(pos - 1);
                } else if (pos == fromPosition) {
                    toUserPhoto = fromUserPhoto;
                } else {//从大往小移动
                    toUserPhoto = mAlbumPhotoAdapter.getItem(pos + 1);
                }
                if (toUserPhoto != fromUserPhoto
                        && fromUserPhoto != null
                        && !TextUtils.isEmpty(fromUserPhoto.getFileUrlMinimum())
                        && toUserPhoto != null
                        && !TextUtils.isEmpty(toUserPhoto.getFileUrlMinimum())
                        ) {
                    reorderImage(String.valueOf(fromUserPhoto.getGuid()), toUserPhoto.getSortIndex());
                }
            }
        };
        ItemDragAndSwipeCallback itemDragAndSwipeCallback = new ItemDragAndSwipeCallback(mAlbumPhotoAdapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemDragAndSwipeCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
        // 开启拖拽
        mAlbumPhotoAdapter.enableDragItem(itemTouchHelper);
        mAlbumPhotoAdapter.setOnItemDragListener(onItemDragListener);

        recyclerView.setAdapter(mAlbumPhotoAdapter);

        mAlbumPhotoAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                UserPhoto item = mAlbumPhotoAdapter.getItem(position);
                if (item != null && !TextUtils.isEmpty(item.getFileUrlMinimum())) {//替换
                    updateImage(String.valueOf(item.getGuid()), position);
                } else {//上传
                    uploadImage(position);
                }
            }
        });
    }

    private void reorderImage(String photoId, String targetIndex) {
        ApiManager.recorderImage(photoId, targetIndex, new IGetDataListener<BaseModel>() {
            @Override
            public void onResult(BaseModel baseModel, boolean isEmpty) {
                EventBus.getDefault().post(new RecordImageEvent());
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
            }
        });
    }

    private void uploadVideoShow(String videoId, final File iconFile, File videoFile, final String duration) {
        mAlbumIview.showLoading();
        ApiManager.uploadVideoShow(videoId, iconFile, videoFile, duration, new IGetDataListener<TUserVideoShow>() {
            @Override
            public void onResult(TUserVideoShow baseModel, boolean isEmpty) {
                mAlbumIview.dismissLoading();
                ToastUtil.showShortToast(mContext, mContext.getString(R.string.upload_success));
                tv_video_time.setText("00:" + (Integer.parseInt(duration) >= 10 ? duration : ("0" + duration)));
                if (item_photo_iv != null) {
                    Glide.with(mContext).load(iconFile).into(item_photo_iv);
                }
                EventBus.getDefault().post(new RecordImageEvent());
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mAlbumIview.dismissLoading();
                ToastUtil.showShortToast(mContext, mContext.getString(R.string.upload_fail));
            }
        });

    }

    @Override
    public void getPhotoInfo() {
        mAlbumIview.showLoading();
        ApiManager.getMyInfo(new IGetDataListener<MyInfo>() {
            @Override
            public void onResult(MyInfo myInfo, boolean isEmpty) {
                if (myInfo != null) {
                    UserDetail userDetail = myInfo.getUserDetail();
                    if (userDetail != null) {
                        mUserPhotos = userDetail.getUserPhotos();
                        mAlbumIview.dismissLoading();
                        mAlbumPhotoAdapter.setNewData(mUserPhotos);
                        if (mAlbumPhotoAdapter.getData().size() == 0 || mAlbumPhotoAdapter.getData().size() < 6) {
                            mAlbumPhotoAdapter.addData(new UserPhoto());
                        }
                        UserBase userBase = userDetail.getUserBase();
                        if (userBase != null) {
                            UserPreference.saveUserInfo(userBase);
                        }
                        if (userDetail.getUserVideoShows() != null && userDetail.getUserVideoShows().size() != 0) {
                            if (item_photo_iv != null) {
                                videoShowId = userDetail.getUserVideoShows().get(0).getId();
                                tv_video_time.setText("00:" + (userDetail.getUserVideoShows().get(0).getVideoSeconds() < 10
                                        ? "0" : "") + userDetail.getUserVideoShows().get(0).getVideoSeconds());
                                Glide.with(mContext)
                                        .load(userDetail.getUserVideoShows().get(0).getThumbnailUrl())
                                        .placeholder(Util.getDefaultImage())
                                        .into(item_photo_iv);
                            }
                        }
                    } else {
                        mAlbumIview.dismissLoading();
                    }
                } else {
                    mAlbumIview.dismissLoading();
                }
            }

            @Override
            public void onError(String msg, boolean isNetworkError) {
                mAlbumIview.dismissLoading();
                if (isNetworkError) {
                    mAlbumIview.showNetworkError();
                } else {
                    mAlbumIview.showTip(msg);
                }
            }
        });
    }

    @Override
    public void uploadImage(final int position) {
        GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
            @Override
            public void getSelectedPhoto(File file) {
                if (null != file) {
                    LoadingDialog.show(mAlbumIview.obtainFragmentManager());
                    ApiManager.upLoadMyPhotoOrAvator(file, false, new IGetDataListener<UpLoadMyPhoto>() {
                        @Override
                        public void onResult(UpLoadMyPhoto upLoadMyPhoto, boolean isEmpty) {
                            LoadingDialog.hide();
                            if (upLoadMyPhoto != null) {
                                final UserPhoto userPhoto = upLoadMyPhoto.getUserPhoto();
                                if (mAlbumPhotoAdapter.getData().size() == 6) {
                                    mAlbumPhotoAdapter.remove(5);
                                }
                                mAlbumPhotoAdapter.addData(position, userPhoto);
                                EventBus.getDefault().post(new RecordImageEvent());
                            }
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                            LoadingDialog.hide();
                            if (isNetworkError) {
                                mAlbumIview.showNetworkError();
                            } else {
                                mAlbumIview.showTip(msg);
                            }
                        }
                    });
                }
            }
        });
    }


    @Override
    public void updateImage(final String photoId, final int position) {
        GetPhotoActivity.toGetPhotoActivity(mContext, new GetPhotoActivity.OnGetPhotoListener() {
            @Override
            public void getSelectedPhoto(File file) {
                if (null != file) {
                    LoadingDialog.show(mAlbumIview.obtainFragmentManager());
                    ApiManager.updateUserPhotoes(file, false, photoId, new IGetDataListener<UpLoadMyPhoto>() {
                        @Override
                        public void onResult(UpLoadMyPhoto upLoadMyPhoto, boolean isEmpty) {
                            LoadingDialog.hide();
                            if (upLoadMyPhoto != null) {
                                final UserPhoto userPhoto = upLoadMyPhoto.getUserPhoto();
                                mAlbumPhotoAdapter.setData(position, userPhoto);
                                EventBus.getDefault().post(new RecordImageEvent());
                            }
                        }

                        @Override
                        public void onError(String msg, boolean isNetworkError) {
                            LoadingDialog.hide();
                            if (isNetworkError) {
                                mAlbumIview.showNetworkError();
                            } else {
                                mAlbumIview.showTip(msg);
                            }
                        }
                    });
                }
            }
        });
    }
}
