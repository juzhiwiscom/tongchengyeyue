package com.wiscomwis.jimo.ui.charmandrankinglist.contract;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import com.wiscomwis.jimo.mvp.BasePresenter;
import com.wiscomwis.jimo.mvp.BaseView;

/**
 * Created by Administrator on 2017/6/15.
 */

public interface VideoListContract {
    interface IView extends BaseView {

        void hideRefresh(int delaySeconds);
        /**
         * 设置VideoListAdapter
         */
        void setAdapter(FragmentPagerAdapter adapter);

        /**
         * 空界面
         */
        void setEmptyView(boolean toggle, String msg);

        /**
         * 错误界面
         */
        void setErrorView(boolean toggle, String msg);
        /**
         * 当前请求页数
         */
        void setPostion();

        /**
         * 刷新，将请求页数置位1
         */

        void clearPosition();

        /**
         * 获取videolistfragment
         */
        Fragment getFragment();

    }


    interface IPresenter extends BasePresenter {

        /**
         * 加载视频秀列表
         */
        void loadVideoShowList(int num);

        /**
         * 举报拉黑
         */
        void reportShow(int position);
    }
}
