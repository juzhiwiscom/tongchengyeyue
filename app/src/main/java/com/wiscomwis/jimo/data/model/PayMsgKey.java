package com.wiscomwis.jimo.data.model;

/**
 * Created by WangYong on 2017/9/16.
 */

public class PayMsgKey extends BaseModel {

    private int payIntercept;

    public int getPayIntercept() {
        return payIntercept;
    }

    public void setPayIntercept(int payIntercept) {
        this.payIntercept = payIntercept;
    }

    @Override
    public String toString() {
        return "PayMsgKey{" +
                "payIntercept=" + payIntercept +
                '}';
    }
}
