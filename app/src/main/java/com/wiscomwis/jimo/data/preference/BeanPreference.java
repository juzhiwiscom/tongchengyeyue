package com.wiscomwis.jimo.data.preference;

import android.content.Context;

import com.wiscomwis.jimo.base.BaseApplication;
import com.wiscomwis.jimo.data.model.UserBean;
import com.wiscomwis.library.util.SharedPreferenceUtil;

/**
 * 保存普通用户账户信息
 * Created by zhangdroid on 2017/6/24.
 */
public class BeanPreference {
    private static Context mContext;

    static {
        mContext = BaseApplication.getGlobalContext();
    }

    private BeanPreference() {
    }

    public static final String NAME = "pre_bean";
    public static final String BEAN_COUNT = "bean_count";

    public static void saveUserBean(UserBean userBean) {
        if (null != userBean) {
            setBeanCount(userBean.getCounts());
        }
    }

    /**
     * 保存用户账户当前金币数
     *
     * @param count
     */
    public static void setBeanCount(int count) {
        SharedPreferenceUtil.setIntValue(mContext, NAME, BEAN_COUNT, count);
    }

    /**
     * @return 用户账户当前金币数
     */
    public static int getBeanCount() {
        return SharedPreferenceUtil.getIntValue(mContext, NAME, BEAN_COUNT, 0);
    }

}
